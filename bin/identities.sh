#! /usr/bin/env bash
set -euo pipefail

# Warning: The following API used in this script is for DBX 3.1
# and can change at any point in the future!!!
# https://confluence.splunk.com/display/PROD/API

SPLUNK_HOST=localhost:8089
SPLUNK_USERNAME="admin"
SPLUNK_PASS="$(cat /mnt/splunk-secrets/password)"

IDENTITY_NAME="pgsql"
DB_USERNAME="$(cat /mnt/jdbc-env/SPRING_DATASOURCE_USERNAME)"
DB_PASSWORD="$(cat /mnt/jdbc-env/SPRING_DATASOURCE_PASSWORD)"


RETRY_MAX=120

for i in $(seq 1 ${RETRY_MAX});
do
  response=$(curl --silent -k -u "${SPLUNK_USERNAME}:${SPLUNK_PASS}"  https://${SPLUNK_HOST}/servicesNS/nobody/splunk_app_db_connect/db_connect/dbxproxy/taskserver | grep -o RUNNING)
  if [ "$response" = 'RUNNING' ]; then
    echo "Taskserver is up"
    break
  fi
  printf '.'
  sleep 2
  if [ "$i" -gt "$RETRY_MAX" ]; then
    echo "Taskserver is not up"
    exit 1
  fi
done


identity_exists() {

  response=$(curl --silent --write-out '%{http_code}' -s -k -u "${SPLUNK_USERNAME}:${SPLUNK_PASS}"  "https://${SPLUNK_HOST}/servicesNS/nobody/splunk_app_db_connect/db_connect/dbxproxy/identities/${1}" --output /dev/null )

  if [[ "$response" -eq "200" ]]; then
    echo T
  else
    echo F
  fi

}

create_identity() {
  curl --silent -k -u "${SPLUNK_USERNAME}:${SPLUNK_PASS}" -H "Content-Type: application/json" -X POST -d '{"name":"'"${1}"'","username":"'"${2}"'", "password":"'"${3}"'"}' https://${SPLUNK_HOST}/servicesNS/nobody/splunk_app_db_connect/db_connect/dbxproxy/identities
}
id_exists=$(identity_exists "${IDENTITY_NAME}")
echo "Identity Exists: $id_exists"

if [ "$id_exists" = "F" ]; then
  echo "Creating Identity: ${IDENTITY_NAME}"
  create_identity ${IDENTITY_NAME} "${DB_USERNAME}" "${DB_PASSWORD}"
fi