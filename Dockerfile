# this version tracks the one used by the operator:
# https://github.com/splunk/splunk-operator/blob/1.0.5/deploy/operator.yaml#L33
FROM splunk/splunk:8.2.3.3
RUN sudo microdnf -y --nodocs install java-11-openjdk-devel
COPY bin /home/splunk/bin
COPY tasks /home/splunk/tasks
RUN sudo chmod -R +x /home/splunk/bin
